package Assignment181122;

import java.util.ArrayList;
import java.util.ListIterator;

public class ArrayListWhileLoop {

	public static void main(String[] args) {
		
		ArrayList<String> ger = new ArrayList<>(); 
		ger.add("Berlin"); 
		ger.add("Hamburg"); 
		ger.add("Munich");
		
		ListIterator<String> itr = ger.listIterator(); 
		System.out.println("Cities in Germany: ");
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}
