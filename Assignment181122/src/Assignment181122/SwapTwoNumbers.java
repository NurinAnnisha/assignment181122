package Assignment181122;

import java.util.Scanner;

public class SwapTwoNumbers {
	
	public static int x, y, temp; 
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);

		//before swapping 
		System.out.println("Enter value x: "); 
		x = scan.nextInt(); 
		
		System.out.println("Enter value y: ");
		y = scan.nextInt(); 
		
		temp = x;
		x = y; 
		y = temp; 
		
		System.out.println("Value of x: " +x); //after swapping 
		System.out.println("Value of y: " +y);	
 }
	
}
