package Assignment181122;

import java.util.Scanner;

public class PrimeNumber {
	
	public static int x; 
		
		public static boolean isPrime(int x) { 
			if(x<=1) {
				return false; //not a prime number 
			}
			
			for (int i=2; i<x/2; i++) { //prime number starts with num 2, check of num is divisable by any number starting from 2 
				//(after num/2, this will still be a prime number)
				if((x%i)==0)  //check for even number 
					return false; 
			}
			return true; 
		}
		
		
		public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in); 
			
			System.out.println("Enter any number/value: ");
			x = scan.nextInt(); 
				
			if(isPrime(x)) { //to check the number whether the value entered is a prime number or not
				System.out.println(x+ " is a prime number");
			} else {
				System.out.println(x+ " is not a prime number");
			}
		}
	}

