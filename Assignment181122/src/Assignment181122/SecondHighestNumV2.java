package Assignment181122;

import java.util.Arrays;

	public class SecondHighestNumV2{
	
		public static void highestNum(int arr[], int arr_size) {
				
		int i; 
				
		Arrays.sort(arr);
		
		for(int x: arr) {
			System.out.println(x); //print all values using for each loop
		}
		
		for (i = arr_size- 2; i >= 0; i--){
			if (arr[i] != arr[arr_size - 1]){
				System.out.printf("Second highest number: " +arr[i]);
				return;
			}
		 }
	   }		

		public static void main(String[] args){
			int arr[] = {401, 402, 403, 404, 405, 406, 407, 408, 409, 410};
			int n = arr.length;
			highestNum(arr, n);
		}
 }

