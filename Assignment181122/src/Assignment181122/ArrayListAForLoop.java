package Assignment181122;

import java.util.ArrayList;

public class ArrayListAForLoop { 

	public static void main(String[] args) {
		
		ArrayList<String> obj = new ArrayList<>(); 		//aka for each loop 
		obj.add("Rome");
		obj.add("Milan"); 
		obj.add("Florance"); 
		
		for(String Italy: obj) {
			System.out.println(Italy);
		}

	}

}
